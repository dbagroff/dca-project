**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

## Atos Glip AZ
1. Hello BitBucket
Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.
2. Pause 111
3. Unpause 333
4. Delete
---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree]

(https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


## BitBucket DCA test
Lorem ipsum
Pause

## ATT Verification has been completed.

## Atos Verification started
Atos bitbucket commit
commit Hercules Pegas OPS

## Hercules
new commit test 1111 222

## Atos Verification 1102
New commit 
Pause commit
Unpause commit
Last commit

## Atos POD33 11/05/2022
New commit
Pause commit
Unpause commit
Last commit

## Atos 27-07-2022
New commit
Pause 1
Unpause 2
Delete

## GLP STG
new commit
pause 1
Unpause
Delete

## Atos 0915
new commit
pause
unpause
delete

## Atos 07-11-22
new commit 
Pause
unpause
unpause 2
delete 

## GLP STG ISTIO
new commit 111
pause
unpause commit
delete

## GLP AWS01 PROD
New commit 0601
pause 
Unpause
delete

## Glip DCA13 2023
new commit 0702 2023
pause
Unpause 2023
Delete

## Glip cross-instance
new commit

## Glip cross OPS STG-AWS
new commit

## FRA04 to RC
new commit
new commit 2

## ATT INC
new commit
new commit 22
new commit 333
new commit 444 

## DDoS protect
new commit 111
new commit 222
new commit 333

## 1805 glip
new commit
new commit 2
new commit 3
new commit 4